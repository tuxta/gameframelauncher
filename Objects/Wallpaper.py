from GameFrame import RoomObject, Globals


class Wallpaper(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image('background.png')
        self.set_image(image, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT)

        self.depth = -100
